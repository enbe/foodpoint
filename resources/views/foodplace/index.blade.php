@extends('layouts.app')

@section('content')
    <a href="{{ route('foodplace.create') }}" class="btn btn-info btn-sm">Tempat Makan</a>
    
    @if ($message = Session::get('message'))
        <div class="alert alert-success martop-sm">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-responsive martop-sm">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Address</th>
            <th>Latitude</th>
            <th>Longtitude</th>
            <th>Description</th>
            <th>Menu ID</th>
        </thead>
        <tbody>
            @foreach ($foodplaces as $foodplace)
                <tr>
                    <td>{{ $foodplace->id }}</td>
                    
                    <td><a href="{{ route('foodplace.show', $foodplace->name) }}">{{ $foodplace->name }}</a>
                    </td>
                    <td><a href="{{ route('foodplace.show', $foodplace->address) }}">{{ $foodplace->address }}</a></td>
                    <td><a href="{{ route('foodplace.show', $foodplace->latitude) }}">{{ $foodplace->latitude }}</a>
                    </td>
                    <td><a href="{{ route('foodplace.show', $foodplace->longtitude) }}">{{ $foodplace->longtitude }}</a>
                    </td>
                    <td><a href="{{ route('foodplace.show', $foodplace->description) }}">{{ $foodplace->description }}</a>
                    </td>
                    <td><a href="{{ route('foodplace.show', $foodplace->menu_id) }}">{{ $foodplace->menu_id }}</a>
                    </td>
                    <td>
                        <form action="{{ route('foodplace.destroy', $foodplace->id) }}" method="post">
                            {{csrf_field()}}
                            {{ method_field('DELETE') }}
                            <a href="{{ route('foodplace.edit', $foodplace->id) }}" class="btn btn-warning btn-sm">Ubah</a>
                            <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection