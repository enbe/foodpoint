extends('layouts.app')

@section('content')
<h4>{{ $foodplace->name }}</h4>
<p>{{  $foodplace->address }}</p>
<p>{{  $foodplace->latitude}}</p>
<p>{{  $foodplace->longtitude}}</p>
<p>{{  $foodplace->description}}</p>
<a href="{{ route('foodplace.index') }}" class="btn btn-default">Kembali</a>
@endsection