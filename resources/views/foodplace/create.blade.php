@extends('layouts.app')

@section('content')
<h4>Tambah Tempat Makan</h4>
<form action="{{ route('foodplace.store') }}" method="post">
    {{csrf_field()}}


    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
        <label for="name" class="control-label">Name</label>
        <input type="text" class="form-control" name="name" >
        @if ($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
        <label for="address" class="control-label">Address</label>
        <textarea name="address" cols="30" rows="5" class="form-control"></textarea>
        @if ($errors->has('address'))
            <span class="help-block">{{ $errors->first('address') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('latitude') ? 'has-error' : '' }}">
        <label for="latitude" class="control-label">Latitude</label>
        <input type="text" class="form-control" name="latitude" >
        @if ($errors->has('latitude'))
            <span class="help-block">{{ $errors->first('latitude') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('longtitude') ? 'has-error' : '' }}">
        <label for="longtitude" class="control-label">Longtitude</label>
        <input type="text" class="form-control" name="longtitude" >
        @if ($errors->has('longtitude'))
            <span class="help-block">{{ $errors->first('longtitude') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
        <label for="description" class="control-label">Description</label>
        <textarea name="description" cols="50" rows="5" class="form-control"></textarea>
        @if ($errors->has('description'))
            <span class="help-block">{{ $errors->first('description') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('menu_id') ? 'has-error' : '' }}">
        <label for="menu_id" class="control-label">Menu_id</label>
        <input type="text" class="form-control" name="menu_id" >
        @if ($errors->has('menu_id'))
            <span class="help-block">{{ $errors->first('menu_id') }}</span>
        @endif
    </div>

    
    <div class="form-group">
        <button type="submit" class="btn btn-info">Simpan</button>
        <a href="{{ route('foodplace.index') }}" class="btn btn-default">Kembali</a>
    </div>
</form>
@endsection