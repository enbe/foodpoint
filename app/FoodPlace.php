<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodPlace extends Model
{
    //
    use SoftDeletes;
 
    protected $fillable = [
        'id','name', 'address', 'latitude','longtitude','description','menu_id'
    ];
    protected $dates = ['deleted_at'];
}
